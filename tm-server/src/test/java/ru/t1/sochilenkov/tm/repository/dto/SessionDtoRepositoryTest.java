package ru.t1.sochilenkov.tm.repository.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.sochilenkov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.dto.IUserDtoService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.marker.UnitCategory;
import ru.t1.sochilenkov.tm.dto.model.SessionDTO;
import ru.t1.sochilenkov.tm.migration.AbstractSchemeTest;
import ru.t1.sochilenkov.tm.service.ConnectionService;
import ru.t1.sochilenkov.tm.service.PropertyService;
import ru.t1.sochilenkov.tm.service.dto.UserDtoService;

import javax.persistence.EntityManager;
import java.util.*;

import static ru.t1.sochilenkov.tm.constant.SessionConstant.*;

@Category(UnitCategory.class)
public class SessionDtoRepositoryTest extends AbstractSchemeTest {

    @NotNull
    private ISessionDtoRepository repository;

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private List<String> userIdList = new ArrayList<>();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(propertyService, connectionService);

    public static String USER_ID;

    public static long USER_ID_COUNTER;

    public static EntityManager entityManager;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");

        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
        entityManager = connectionService.getEntityManager();
    }

    @Before
    public void init() {
        repository = new SessionDtoRepository(entityManager);
        sessionList = new ArrayList<>();
        userIdList = new ArrayList<>();
        entityManager.getTransaction().begin();
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            USER_ID_COUNTER++;
            USER_ID = userService.create("session_rep_usr_" + USER_ID_COUNTER, "1").getId();
            userIdList.add(USER_ID);
        }
        for (int i = 0; i < INIT_COUNT_SESSIONS; i++) {
            @NotNull final SessionDTO session = new SessionDTO();
            session.setUserId(userIdList.get(i));
            session.setRole(Role.USUAL);
            repository.add(userIdList.get(i), session);
            sessionList.add(session);
        }
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
    }

    @After
    public void ClearAfter() {
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        for (@NotNull final String userId : userIdList) {
            repository.clear(userId);
        }
        entityManager.getTransaction().commit();
        userService.clear();
    }

    @AfterClass
    public static void closeConnection() {
        entityManager.close();
    }

    @Test
    public void testAddSessionPositive() {
        @NotNull SessionDTO session = new SessionDTO();
        session.setUserId(UUID.randomUUID().toString());
        session.setRole(Role.USUAL);
        repository.add(userIdList.get(0), session);
        Assert.assertEquals(2, repository.getSize(userIdList.get(0)));
    }

    @Test
    public void testClear() {
        for (@NotNull final String userId : userIdList) {
            Assert.assertEquals(1, repository.getSize(userId));
            repository.clear(userId);
            Assert.assertEquals(0, repository.getSize(userId));
        }
    }

    @Test
    public void testFindById() {
        for (@NotNull final String userId : userIdList) {
            Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
        }
        for (@NotNull final SessionDTO session : sessionList) {
            final SessionDTO foundSession = repository.findOneById(session.getUserId(), session.getId());
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testExistsById() {
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        for (@NotNull final SessionDTO session : sessionList) {
            Assert.assertTrue(repository.existsById(session.getId()));
        }
    }

    @Test
    public void testFindByIndex() {
        for (@NotNull final SessionDTO session : sessionList) {
            Assert.assertNull(repository.findOneByIndex(session.getUserId(), 9999));
            final SessionDTO foundSession = repository.findOneByIndex(session.getUserId(), 1);
            Assert.assertNotNull(foundSession);
            Assert.assertEquals(session.getId(), foundSession.getId());
        }
    }

    @Test
    public void testFindAll() {
        for (@NotNull final String userId : userIdList) {
            List<SessionDTO> sessions = repository.findAll(userId);
            Assert.assertNotNull(sessions);
            Assert.assertEquals(1, sessions.size());
            for (@NotNull final SessionDTO session : sessionList) {
                if (session.getUserId().equals(userId))
                    Assert.assertNotNull(
                            sessions.stream()
                                    .filter(m -> session.getId().equals(m.getId()))
                                    .filter(m -> session.getUserId().equals(m.getUserId()))
                                    .findFirst()
                                    .orElse(null));
            }
        }
    }

    @Test
    public void testRemoveById() {
        for (@NotNull final SessionDTO session : sessionList) {
            repository.removeById(session.getUserId(), session.getId());
            Assert.assertNull(repository.findOneById(session.getUserId(), session.getId()));
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test
    public void testRemove() {
        for (final SessionDTO session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            repository.remove(session.getUserId(), session);
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

    @Test
    public void testRemoveWOUserId() {
        for (final SessionDTO session : sessionList) {
            Assert.assertEquals(1, repository.getSize(session.getUserId()));
            repository.remove(session);
            Assert.assertEquals(0, repository.getSize(session.getUserId()));
        }
    }

}
