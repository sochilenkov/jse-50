package ru.t1.sochilenkov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.model.IProjectRepository;
import ru.t1.sochilenkov.tm.api.repository.model.ITaskRepository;
import ru.t1.sochilenkov.tm.api.repository.model.IUserRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.IPropertyService;
import ru.t1.sochilenkov.tm.api.service.model.IUserService;
import ru.t1.sochilenkov.tm.enumerated.Role;
import ru.t1.sochilenkov.tm.exception.entity.EntityNotFoundException;
import ru.t1.sochilenkov.tm.exception.entity.UserNotFoundException;
import ru.t1.sochilenkov.tm.exception.field.*;
import ru.t1.sochilenkov.tm.model.User;
import ru.t1.sochilenkov.tm.repository.model.ProjectRepository;
import ru.t1.sochilenkov.tm.repository.model.TaskRepository;
import ru.t1.sochilenkov.tm.repository.model.UserRepository;
import ru.t1.sochilenkov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService implements IUserService {

    IConnectionService connectionService;

    IPropertyService propertyService;

    public UserService(@NotNull IPropertyService propertyService,
                       @NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @SneakyThrows
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user = new User();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull User user = new User();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            if (repository.isLoginExist(login)) throw new ExistsLoginException();
            if (password == null || password.isEmpty()) throw new PasswordEmptyException();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(Role.USUAL);
            if (email != null && !email.isEmpty()) {
                if (repository.isEmailExist(email)) throw new ExistsEmailException();
                user.setEmail(email);
            }
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull User user = new User();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            if (repository.isLoginExist(login)) throw new ExistsLoginException();
            if (password == null || password.isEmpty()) throw new PasswordEmptyException();
            if (role == null) throw new RoleEmptyException();
            user.setLogin(login);
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            user.setRole(role);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable User user) {
        if (user == null) throw new EntityNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public @NotNull List<User> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull List<User> result;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            result = repository.findAll();
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findOneById(id);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findByLogin(login);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findByEmail(email);
        } finally {
            entityManager.close();
        }
        return user;
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            user = repository.findByEmail(email);
            if (user == null) throw new UserNotFoundException();
            entityManager.getTransaction().begin();
            repository.remove(user);
            taskRepository.clear(user.getId());
            projectRepository.clear(user.getId());
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(user);
            @NotNull final String userId = user.getId();
            taskRepository.clear(userId);
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findOneById(id);
            if (user == null) throw new UserNotFoundException();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setMiddleName(middleName);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        boolean result;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            result = repository.isLoginExist(login);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        boolean result;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            result = repository.isEmailExist(email);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable User user;
        try {
            @NotNull final IUserRepository repository = new UserRepository(entityManager);
            user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(false);
            entityManager.getTransaction().begin();
            repository.update(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
