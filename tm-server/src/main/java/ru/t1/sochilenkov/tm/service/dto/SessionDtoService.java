package ru.t1.sochilenkov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.sochilenkov.tm.api.service.IConnectionService;
import ru.t1.sochilenkov.tm.api.service.dto.ISessionDtoService;
import ru.t1.sochilenkov.tm.dto.model.SessionDTO;
import ru.t1.sochilenkov.tm.exception.field.IdEmptyException;
import ru.t1.sochilenkov.tm.exception.field.IndexIncorrectException;
import ru.t1.sochilenkov.tm.exception.field.UserIdEmptyException;
import ru.t1.sochilenkov.tm.exception.system.AuthenticationException;
import ru.t1.sochilenkov.tm.repository.dto.SessionDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionDtoService implements ISessionDtoService {

    @NotNull IConnectionService connectionService;

    public SessionDtoService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull List<SessionDTO> result;
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            result = repository.findAll(userId);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        boolean result;
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            result = repository.existsById(id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable SessionDTO result;
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            result = repository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable SessionDTO result;
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            result = repository.findOneByIndex(userId, index);
        } finally {
            entityManager.close();
        }
        return result;
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @Nullable SessionDTO session;
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            session = repository.findOneByIndex(userId, index);
            if (session == null) return;
            entityManager.getTransaction().begin();
            repository.remove(userId, session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull SessionDTO session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.remove(session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void add(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new AuthenticationException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(userId, session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public SessionDTO add(@NotNull final SessionDTO session) {
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ISessionDtoRepository repository = new SessionDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(session.getUserId(), session);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

}
